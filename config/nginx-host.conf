server {
    listen 80;
    server_name default.domain.com;
    
    root /var/www/html/default;
    index index.html index.htm index.php;
  
    access_log /var/log/nginx/default.domain.com-access.log;
    error_log /var/log/nginx/default.domain.com-error.log;
    
    client_max_body_size 128M;

    # # redirect non-www to www
    # if ($host !~ ^www\.) {
    #         rewrite ^ $scheme://www.$host$request_uri permanent;
    # }

    ## redirect to HTTPS behind ELB
    # if ($http_x_forwarded_proto != 'https') {
    #       return 301 https://$host$request_uri;
    # }
    
    # default path
    location / {
            try_files $uri $uri/ /index.php?$args;
    }

    # health check path
    location /nginx-health {
            access_log off;
            return 200 "healthy\n";
    }

    # block favicon log
    location = /favicon.ico {
            log_not_found off;
            access_log off;
    }

    # block or allow search engines
    location = /robots.txt {
            add_header Content-Type text/plain;
            return 200 "User-agent: *\nDisallow: /\n";
            access_log off;
    }

    # cache static
    location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
            expires 30d;
            add_header Pragma public;
            add_header Cache-Control "public";
    }

    # block extensions
    location ~* \.(log|sh|sql)$|/\. {
            deny all;
    }

    # php-fpm
    location ~ \.php$ {
            fastcgi_index index.php;
            fastcgi_intercept_errors on;
            fastcgi_split_path_info ^(.+\.php)(/.+)$;
            fastcgi_pass 127.0.0.1:9000;
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
            fastcgi_buffers 16 16k;
            fastcgi_buffer_size 32k;
            }

}
