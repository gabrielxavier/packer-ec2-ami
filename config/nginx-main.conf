user nginx;
worker_processes auto;
pid /var/run/nginx.pid;

error_log /var/log/nginx/error.log info;

worker_rlimit_nofile 1024;

events {
    worker_connections 1024;
    # multi_accept on;
}

http {
    access_log /var/log/nginx/access.log;

    open_file_cache max=1024 inactive=10s;
    open_file_cache_valid 120s;

    server_tokens off;
    
    set_real_ip_from  0.0.0.0/0;
    real_ip_header    X-Forwarded-For;
    real_ip_recursive on;

    sendfile on;
    autoindex off;
    keepalive_timeout 65;
    include /etc/nginx/mime.types;
    default_type application/octet-stream;
    types {
        font/woff2;
    }
    
    client_max_body_size 32M;

    proxy_connect_timeout       600;
    proxy_send_timeout          600;
    proxy_read_timeout          600;
    send_timeout                600;
    proxy_ignore_client_abort   on;

    gzip  on;
    gzip_comp_level 2;
    gzip_proxied any;
    gzip_types text/plain text/css application/x-javascript text/xml application/xml application/xml+rss text/javascript;

    include /etc/nginx/conf.d/*.conf;

server {
    listen 80 default;
    server_name _; 
    root /var/www/html;
    index index.html;

    location /nginx-health {
        access_log off;
        return 200 "healthy\n";
        }
    }
}
