#!/bin/bash
# Ubuntu 18.04 + PHP7.2 + Nginx 1.14

# variables
PHP_VER=7.2

# applications
apt update -q
apt install -qy software-properties-common wget apt-utils
wget http://nginx.org/keys/nginx_signing.key
apt-key add nginx_signing.key
sh -c "echo 'deb http://nginx.org/packages/ubuntu/ '$(lsb_release -cs)' nginx' > /etc/apt/sources.list.d/nginx.list"
add-apt-repository ppa:ondrej/php -y
apt update -q
DEBIAN_FRONTEND=noninteractive apt install -qy \
        language-pack-pt \
        bash-completion \
        ca-certificates \
        cron \
        curl \
        g++ \
        git \
        goaccess \
        graphviz \
        libcurl3-openssl-dev \
        libmysqlclient-dev \
        htop \
        make \
        mysql-client \
        nginx \
        openssl \
        php-pear \
        php${PHP_VER} \
        php${PHP_VER}-bcmath \
        php${PHP_VER}-cgi \
        php${PHP_VER}-cli \
        php${PHP_VER}-common \
        php${PHP_VER}-curl \
        php${PHP_VER}-dev \
        php${PHP_VER}-fpm \
        php${PHP_VER}-gd \
        php${PHP_VER}-mbstring \
        php${PHP_VER}-mysql \
        php${PHP_VER}-opcache \
        php${PHP_VER}-soap \
        php${PHP_VER}-xml \
        php${PHP_VER}-zip \
        postfix \
        python-dev \
        python-pip \
        pwgen \
        ruby-dev \
        tzdata \
        unzip \
        vim \
        wget \
        zip 

# create web folders
mkdir -p /var/www/html/default
cp /tmp/config/index.html /var/www/html/index.html
cp /tmp/config/index.php /var/www/html/default/index.php

# ansible
pip install ansible MySQL-python boto boto3

# start processes on startup
systemctl enable nginx php${PHP_VER}-fpm postfix

# time/date
echo "America/Sao_Paulo" | tee /etc/timezone
dpkg-reconfigure --frontend noninteractive tzdata

# create additional user
useradd -m -s /bin/bash suporte
usermod -aG sudo suporte
usermod -aG admin suporte
usermod -aG nginx suporte
sudo -u suporte mkdir -p -m 700 /home/suporte/.ssh
sudo -u suporte touch /home/suporte/.ssh/authorized_keys && chmod 600 /home/suporte/.ssh/authorized_keys
sudo -u suporte sh -c "cat /tmp/config/ssh-keys/suporte.pub > /home/suporte/.ssh/authorized_keys"
echo "suporte ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers
chown -R suporte:suporte /home/suporte

# fix permissions
chmod -R 777 /var/www
chown -R nginx:nginx /var/www

# config files
cp /tmp/config/nginx-main.conf /etc/nginx/nginx.conf
cp /tmp/config/nginx-host.conf /etc/nginx/conf.d/default.conf
cp /tmp/config/php-fpm.conf /etc/php/${PHP_VER}/fpm/pool.d/zzz.conf 
cp /tmp/config/php.ini /etc/php/${PHP_VER}/fpm/conf.d/zzz.ini
