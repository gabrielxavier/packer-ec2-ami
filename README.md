# Packer EC2 AMI Example

#### Webserver

- Ubuntu 18.04
- Nginx 1.14
- PHP-FPM 7.2

#### How to use
```shell
packer build packer.json
```